<?php

declare(strict_types=1);

namespace Paneric\CSRTriad\Controller;

use Twig\Environment as Twig;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

abstract class AppController
{
    protected $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    protected function render(Response $response, string $template, array $data = [], int $status = null): Response
    {
        try {
            $response->getBody()->write(
                $this->twig->render($template, $data)
            );
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            echo sprintf(
                '%s%s%s%s',
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            );
        }

        $response = $response->withHeader('Content-Type', 'text/html;charset=utf-8');

        if ($status !== null) {
            return $response->withStatus($status);
        }

        return $response;
    }

    protected function redirect(Response $response, string $url, int $status = null): Response
    {
        $response = $response->withHeader('Location', (string)$url);

        if ($status !== null) {
            return $response->withStatus($status);
        }

        return $response;
    }
}
