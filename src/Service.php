<?php

declare(strict_types=1);

namespace Paneric\CSRTriad;

use Paneric\Interfaces\Session\SessionInterface;

abstract class Service
{
    protected $session;

    public function __construct(SessionInterface $session = null)
    {
        $this->session = $session;
    }

    public function jsonSerializeObjects(array $collection, bool $isCoc = false): ?array
    {
        if ($isCoc === false) {
            foreach ($collection as $key => $object) {
                $collection[$key] = is_array($object) ? $object : $object->convert();
            }

            return $collection;
        }

        foreach ($collection as $keyC => $objects) {
            $objects = $objects->convert();

            foreach ($objects as $key => $object) {
                $objects[$key] = is_array($object) ? $object : $object->convert();
            }

            $collection[$keyC] = $objects;
        }

        return $collection;
    }

    public function jsonSerializeObjectsById(array $objects): ?array
    {
        $objectsById = [];

        foreach ($objects as $object) {
            $key = is_array($object) ? $object['id'] : $object->getId();
            $objectsById[$key] = is_array($object) ? $object : $object->convert();
        }

        return $objectsById;
    }

    public function jsonSerializeArrays(array $attributes, String $class, bool $converted = true): array
    {
        $attributes = (array_filter($attributes, static function($attribute)
        {
            return is_array($attribute);
        }));

        $keys = array_keys($attributes);

        $values = [];
        $serializedValues = [];

        foreach ($attributes[$keys[0]] as $index => $attribute) {
            foreach ($keys as $key) {
                $values[$key] = $attributes[$key][$index];
            }

            $hydrator = new $class();
            $hydrator->hydrate($values);

            $hydratorConverted = $hydrator->convert();

            if (!empty($hydratorConverted)) {
                if ($converted) {
                    $serializedValues[] = $hydratorConverted;
                } else {
                    $serializedValues[] = $hydrator;
                }

            }
        }

        return $serializedValues;
    }

    public function hydrateArrayCollection(array $arrayCollection, String $class): array
    {
        foreach ($arrayCollection as $index => $array) {
            $hydrator = new $class();
            $hydrator->hydrate($array);

            $arrayCollection[$index] = $hydrator;
        }

        return $arrayCollection;
    }

    public function convertObjectCollection(array $objectCollection): array
    {
        foreach ($objectCollection as $index => $object) {
            $objectCollection[$index] = $object->convert();
        }

        return $objectCollection;
    }

    protected function encodeFileName(string $fileName): string
    {
        $fileName = str_replace('.', '%', $fileName);

        return urlencode($fileName);
    }

    protected function decodeFileName(string $fileName, string $extension = null): string
    {
        $fileName = str_replace('%', '.', $fileName);

        $fileName = urldecode($fileName);

        return $extension === null ? $fileName : $fileName . $extension;
    }
}
