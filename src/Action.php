<?php

declare(strict_types=1);

namespace Paneric\CSRTriad;

use Paneric\Interfaces\DataObject\DataObjectInterface;
use Paneric\Interfaces\Session\SessionInterface;

class Action
{
    protected $session;

    public function __construct(SessionInterface $session = null)
    {
        $this->session = $session;
    }

    public function createCollectionFromAttributes(array $attributes, string $daoClass, bool $hydrate = false): array
    {
        $attributes = (array_filter($attributes, static function($attribute)
        {
            return is_array($attribute);
        }));

        $keys = array_keys($attributes);

        $values = [];
        $collection = [];

        foreach ($attributes[$keys[0]] as $index => $attribute) {
            foreach ($keys as $key) {
                $values[$key] = $attributes[$key][$index];
            }

            $hydrator = new $daoClass();
            $hydrator->hydrate($values);

            $hydratorConverted = $hydrator->convert();

            if (!empty($hydratorConverted)) {
                if (!$hydrate) {
                    $collection[$index] = $hydratorConverted;
                } else {
                    $collection[$index] = $hydrator;
                }

            }
        }

        return $collection;
    }

    public function arrangeArraysCollectionById(array $collection, bool $hydrate = false, string $dtoClass = null): array
    {
        $itemsById = [];

        foreach ($collection as $item) {
            $key = $item['id'];

            if (!$hydrate) {
                $itemsById[$key] = $item;
            }

            if ($hydrate) {
                $dto = new $dtoClass();
                $dto->hydrate($item);
                $itemsById[$key] = $dto;
            }
        }

        return $itemsById;
    }

    public function arrangeObjectsCollectionById(array $collection, bool $convert = false): array
    {
        $itemsById = [];

        foreach ($collection as $item) {
            $key = $item->getId();

            if ($convert) {
                $itemConverted = $item->convert();

                foreach ($itemConverted as $ick => $icv) {
                    if ($icv instanceof DataObjectInterface) {
                        $itemConverted[$ick] = $icv->convert();
                    }
                }

                $itemsById[$key] = $itemConverted;
            }

            if (!$convert) {
                $itemsById[$key] = $item;
            }
        }

        return $itemsById;
    }

    public function arrangeObjectsCollection(array $collection, bool $convert = false): array
    {
        $items = [];

        foreach ($collection as $index => $item) {
            if ($convert) {
                $itemConverted = $item->convert();

                foreach ($itemConverted as $ick => $icv) {
                    if ($icv instanceof DataObjectInterface) {
                        $itemConverted[$ick] = $icv->convert();
                    }
                }

                $items[] = $itemConverted;
            }

            if (!$convert) {
                $items[] = $item;
            }
        }
        return $items;
    }

    public function hydrateCollection(array $collection, String $class): array
    {
        foreach ($collection as $index => $array) {
            $hydrator = new $class();
            $hydrator->hydrate($array);

            $collection[$index] = $hydrator;
        }

        return $collection;
    }

    public function convertCollection(array $collection): array
    {
        foreach ($collection as $index => $object) {
            $collection[$index] = $object->convert();
        }

        return $collection;
    }

    public function getCollectionIds(array $collection): array
    {
        foreach ($collection as $index => $object) {
            $collection[$index] = $object->getId();
        }

        return $collection;
    }
}
